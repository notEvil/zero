**!! Abandoned !!**

I never used those scripts to "easy" install applications and my configurations on Windows.
In the future I aim to provide my configurations at [https://gitlab.com/notEvil/notevil](https://gitlab.com/notEvil/notevil) in a way that is easy to use once the corresponding applications are installed.

# zero

.. for now, is a messy set of scripts for Windows. Their purpose: install (Python) stuff automatically.

### How

Just execute any public\* Batch script and it will install the corresponding part and its dependencies right there without any further intervention (usually). All system-wide dependencies like Python are installed using [_winget_](https://github.com/microsoft/winget-cli) with user privileges. All Python requirements are installed in virtual environments using [_Pipenv_](https://github.com/pypa/pipenv).

The Batch script is a non-functional layer around Powershell which manages the install process for Python. It then executes a Python script to do the rest. All parts share a single virtual environment for preparation, but each might set up their own virtual environment for execution.

\* files starting with `_` are private/internal

### Why

Windows users expect a single step installation process. But all I want is to provide a Python package with a few dependencies in a virtual environment. Seems easy enough, just provide a script which installs Python and _Pipenv_. What seems so simple turned out rather messy because

- Batch scripts are just pain
  - e.g. insane rules for quotes, errors are ignored
- Windows denies execution of foreign Powershell scripts
  - but allows execution of foreign Batch scripts which may execute foreign Powershell scripts (why? idk)
- Windows has a package manager called _winget_ which is available in the Microsoft Store, but it can't be installed automatically from a script
- Windows uses `C:\Windows\System32` as the working directory when executing scripts from the File Explorer


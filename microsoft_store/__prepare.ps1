$ErrorActionPreference = "Inquire"
echo '> installing Microsoft Store'
$name = Get-AppxPackage -Allusers | Out-String -Stream | Select-String -Pattern "(?<=^PackageFullName\s*:\s*)Microsoft\.WindowsStore_.*$" | Foreach-Object { $_.Matches[0].Value }
Add-AppxPackage -register "C:\Program Files\WindowsApps\$name\AppxManifest.xml" -DisableDevelopmentMode
echo '> Done'
pause

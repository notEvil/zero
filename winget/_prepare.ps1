$ErrorActionPreference = "Inquire"
if ((Get-Command winget -ErrorAction SilentlyContinue) -eq $null) {
  echo '> installing Winget'
  echo "I couldn't find a way to install Winget automatically"
  echo 'You need to open the Microsoft Store app, search for "App Installer" and install it'
  echo 'If you uninstalled the Microsoft Store app, execute microsoft_store/prepare.bat'
  pause
}

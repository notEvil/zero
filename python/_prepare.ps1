$ErrorActionPreference = "Inquire"
powershell -File "$PSScriptRoot\..\winget\_prepare.ps1"; if (!$?) { throw }
winget list -q python
if (!$?) {
  echo '> installing Python'
  winget install python; if (!$?) { throw }
}

import subprocess
import sys


try:
    import pipenv

except ImportError:
    print("> installing Pipenv")
    _ = subprocess.Popen([sys.executable, "-m", "pip", "install", "pipenv"])
    assert _.wait() == 0

import pathlib
import subprocess
import sys


path = pathlib.Path(".").resolve()

while True:
    if pathlib.Path(path, "Pipfile").exists():
        break

    if path.parent == path:
        raise Exception
    path = path.parent


# INFO stdout=subprocess.DEVNULL doesn't work on Windows
return_code = subprocess.Popen([sys.executable, "-m", "pipenv", "--venv"]).wait()
assert return_code in [0, 1]

if return_code == 1:
    print("> preparing Python environment")

    lock_path = pathlib.Path(path, "Pipfile.lock")
    if lock_path.exists():
        assert subprocess.Popen([sys.executable, "-m", "pipenv", "sync"]).wait() == 0

    else:
        try:
            _ = [sys.executable, "-m", "pipenv", "install"]
            assert subprocess.Popen(_).wait() == 0

        finally:
            lock_path.unlink()

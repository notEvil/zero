import pathlib
import subprocess
import sys


PATH = pathlib.Path(__file__).parent


_ = str(pathlib.Path(PATH.parent, "pipenv", "prepare_environment.py"))
assert subprocess.Popen([sys.executable, _], cwd=PATH).wait() == 0

_ = str(pathlib.Path(PATH, "__prepare.py"))
_ = [sys.executable, "-m", "pipenv", "run", "python", _]
assert subprocess.Popen(_, cwd=PATH).wait() == 0

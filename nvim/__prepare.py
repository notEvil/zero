import requests
import os
import pathlib
import shutil
import subprocess


PATH = pathlib.Path(__file__).parent


if not pathlib.Path(r"C:\Program Files\Neovim\bin\nvim-qt.exe").exists():
    _ = ["winget", "install", "Microsoft.VCRedist.2015+.x64"]
    assert subprocess.Popen(_).wait() == 0
    assert subprocess.Popen(["winget", "install", "Neovim.Neovim"]).wait() == 0

if shutil.which("git") is None:
    assert subprocess.Popen(["winget", "install", "Git.Git"]).wait() == 0
    _ = os.environ["PATH"].split(";") + [r"C:\Program Files\Git\cmd"]
    os.environ["PATH"] = ";".join(_)

path = pathlib.Path(PATH, "installer.ps1")
_ = requests.get(
    "https://raw.githubusercontent.com/Shougo/dein.vim/master/bin/installer.ps1",
    stream=True,
)
with open(path, "wb") as file:
    for bytes in _.iter_content(chunk_size=4096):
        file.write(bytes)

_ = ["powershell", "-ExecutionPolicy", "Bypass", "-File", str(path), "~/.cache/dein"]
assert subprocess.Popen(_).wait() == 0

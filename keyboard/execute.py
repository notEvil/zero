import argparse
import os
import pathlib
import subprocess
import sys


PATH = pathlib.Path(__file__).parent


parser = argparse.ArgumentParser()

parser.add_argument("--mpw", default=False, action="store_true")

arguments, remaining_arguments = parser.parse_known_args()


path = pathlib.Path(PATH, "keyboard-master")
windows_path = pathlib.Path(path, "windows")

if arguments.mpw:
    _ = [sys.executable, str(pathlib.Path(windows_path, "_prepare_mpw.py"))]
    assert subprocess.Popen(_).wait() == 0

print("> running")
env = os.environ | dict(
    PYTHONPATH=";".join(map(str, [path, pathlib.Path(path, "keyboard", "windows")]))
)
_ = [sys.executable, "-m", "pipenv", "run", "python"] + remaining_arguments
assert subprocess.Popen(_, cwd=windows_path, env=env).wait() == 0

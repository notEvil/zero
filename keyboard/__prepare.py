import requests
import os
import pathlib
import shutil
import subprocess
import sys
import zipfile


PATH = pathlib.Path(__file__).parent


path = pathlib.Path(PATH, "keyboard-master.zip")

print("> downloading notEvil/keyboard")
_ = requests.get(
    "https://gitlab.com/notEvil/keyboard/-/archive/master/keyboard-master.zip",
    stream=True,
)
with open(path, "wb") as file:
    for bytes in _.iter_content(chunk_size=4096):
        file.write(bytes)

print("> extracting notEvil/keyboard")
with zipfile.ZipFile(path) as zip_file:
    zip_file.extractall(PATH)

_ = str(pathlib.Path(PATH, "keyboard-master", "windows", "_prepare.py"))
assert subprocess.Popen([sys.executable, _], cwd=PATH).wait() == 0
